/*
   Welcome to Odinlings (absolutely a ripoff of Ziglings)

   Every file in odin starts with a `package` declaration, though, this one
   seems to be missing such a declaration. Package declarations always follow
   the form of:
   ```odin
   package <package name>;
   ```
   Semi-colons are always optional in Odin.
*/

import "core:fmt";

main :: proc()
{
	fmt.println("Hellope!");
}
