/*
   Most programming languages have STDs, but Odin doesn't. No one wants them.
   Odin's solution is the core collection.

   You import packages with the `import` keyword followed by a string literal
   of the format `[collection][:]<file path>`. Built in collections include
   `core`, `builtin` and `vendor`. Additional collections can be added with
   compiler options.
*/
package main;

import "???:fmt";

main :: proc()
{
	fmt.println("No STDs here!");
}
