#!/bin/bash


RunExercise () {
	EXPECTED_STDOUTS=(
		"Hellope!"
		"No STDs here!"
	);
	ODIN_COMPILER="odin";
	FLAGS="-file";
	. <(
		{
			ERR=$(
				{
					OUTPUT=$($ODIN_COMPILER run $1 "-out:$2" $FLAGS);
					RES=$?;
				} 2>&1;
				declare -p OUTPUT >&2;
				declare -p RES >&2;
			);
			declare -p ERR;
		} 2>&1;
	)
	if [[ $RES == 0 ]]; then
		if [[ "$OUTPUT" == ${EXPECTED_STDOUTS[$3]} ]]; then
			printf "\033[1;32m$(echo $1 | cut -d'/' -f2)\033[0m\n";
			printf "\033[1;32m$OUTPUT\033[0m\n";
		else
			printf "\033[1;31m$(echo $1 | cut -d'/' -f2)\033[0m\n";
			printf "\033[1;31m$OUTPUT\033[0m\n";
			exit 255;
		fi
	else
		printf "\033[1;31m$(echo $1 | cut -d'/' -f2)\033[0m\n";
		printf "\033[1;31m$ERR\033[0m\n";
		exit 255;
	fi
}

if [[ $1 ]]; then
	echo $1
	EXERCISE=$(find src -type f | grep $1);
	BIN_NAME=$(echo $EXERCISE | cut -d'/' -f2 | cut -d'.' -f1)
	echo $BIN_NAME
	RunExercise $EXERCISE "build/debug/bin/$BIN_NAME" $1;
	exit 0;
else
	ITER=0;
	rm -rf 'build'
	mkdir -p 'build/debug/bin'
	for i in $(find src -type f | sort);
	do
		BIN_NAME=$(echo $i | cut -d'/' -f2 | cut -d'.' -f1)
		RunExercise $i "build/debug/bin/$BIN_NAME" $ITER
		ITER=$((ITER + 1));
	done
fi
